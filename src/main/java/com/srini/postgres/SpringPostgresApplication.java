package com.srini.postgres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.annotation.Id;

/**
 * The type Spring postgres application.
 */
@SpringBootApplication
public class SpringPostgresApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringPostgresApplication.class, args);
	}

}


