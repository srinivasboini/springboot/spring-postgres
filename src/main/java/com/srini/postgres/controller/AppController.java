package com.srini.postgres.controller;

import com.srini.postgres.entity.Employees;
import com.srini.postgres.repository.AppRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * The type App controller.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class AppController {

    private final AppRepository appRepository ;

    /**
     * Execute.
     */
    @EventListener(ApplicationStartedEvent.class)
    public void execute(){
        appRepository.save(new Employees(0,"srini", 34));
        appRepository.save(new Employees(0,"Le Thi Ha", 34));
        List<Employees> employeesList = appRepository.findAll() ;
        System.out.println(employeesList);
    }

    /**
     * Get all employees list.
     *
     * @return the list
     */
    @GetMapping("all")
    public List<Employees> getAllEmployees(){
        return appRepository.findAll() ;
    }

    /**
     * Get employee optional.
     *
     * @param id the id
     * @return the optional
     */
    @GetMapping("/get/{id}")
    public Optional<Employees> getEmployee(@PathVariable Integer id){
        return appRepository.findById(id) ;
    }


    /**
     * Save employee response entity.
     *
     * @param employees the employees
     * @return the response entity
     */
    @PostMapping("/save")
    public ResponseEntity<HttpStatus> saveEmployee(@RequestBody Employees employees){
        appRepository.save(employees) ;
        return new ResponseEntity<>(HttpStatus.ACCEPTED) ;
    }

    /**
     * Save employee response entity.
     *
     * @param id the id
     * @return the response entity
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> saveEmployee(@PathVariable Integer id){
        appRepository.deleteById(id); ;
        return new ResponseEntity<>(HttpStatus.OK) ;
    }
}
