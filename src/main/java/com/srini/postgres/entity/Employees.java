package com.srini.postgres.entity;

import org.springframework.data.annotation.Id;

/**
 * The type Employees.
 */
public record Employees(@Id int id, String name, int age){}