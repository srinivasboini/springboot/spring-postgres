package com.srini.postgres.repository;

import com.srini.postgres.entity.Employees;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface App repository.
 */
@Repository
public interface AppRepository extends ListCrudRepository<Employees, Integer> {
}
